# Génère une clé SSH
ssh-keygen -t rsa -b 4096 -C "youremail@domain.com"

# Déplacement dans le dossier .ssh contenant les clés
cd ~/.ssh

# Affiche la clé publique dans le terminal
cat id_rsa.pub 